# example d'un package d'ip avec Vivado

__Instructions pour la simulation__
----

1) Installer les dépendances
``` bash
sudo apt install gtkwave ghdl
```

----
2) Lancer la(les) simulation
``` bash
make ghdl_sim
```

----
3) Visualiser le résultat des simulations
``` bash
gtkwave <testbench_name>.vcd
```

__Instructions pour le packaging dans vivado__
----

1) S'assurer de bien avoir les outils de Xilinx dans le PATH
``` bash
source <vivado install dir>/settings64.sh
```

----
2) Packager l'IP
``` bash 
make vivado_all
```

----
3) Importer dans Vivado

Cliquez sur "Project-\>Setting". Puis deployez la liste "IP". Puis dans "Repository" choisir le chemin vers le dossier "ip\_repo" généré.

__Autres Commandes Utiles__
----

- La commande `make` exécute la compilation des cibles pour ghdl ainsi que la génération de l'IP Vivado.
- La commande `make clean` supprime les produits de compilation et de simulation GHDL ainsi que l'IP Vivado produite.
- Pour supprimer les produits de compilation et de simulation GHDL, utiliser la commande `make ghdl_clean`.
- De même, pour supprimer l'IP vivado produite, utiliser la commande `make vivado_clean`.
- Pour lancer uniquement le build GHDL sans lancer la simulation, utiliser `make ghdl_all`.
- Si vous voulez lancer des simulations à la main, GHDL va créer un executable par testbench. Pour en connaître les arguments possibles: https://ghdl.readthedocs.io/en/latest/using/Simulation.html

__Comment apporter des modifications__
----

- Les sources vhdl pour la synthèse doivent être placés dans le dossier synth.
- Les sources vhdl pour la simulation (testbench) doivent être placés dans le dossier sim.
- Si vous voulez packager plusieurs IPs ajoutez les sources dans synth/ et sim/ et créez un autre script tcl dans script/vivado (vous n'avez que 4 lignes à modifier par rapport au script simple\_adder.tcl)
- Si vous modifiez le nom des sources vous devrez apporter des modification au script tcl correspondant dans le dossier script/vivado/

__Important__
----

- Bien respecter les nommages des différents standards (si vous n'ajoutez pas de prefix vous pouvez vous passer du "\_"):
  - pour l'axi stream (slave) : \<prefix\>\_tdata, \<prefix\>\_tready, \<prefix\>\_tvalid, ... 
  - pour les clocks :  \<prefix\>\_aclk,  \<prefix\>\_clk, \<prefix\>\_clock
  - pour les resets :  \<prefix\>\_areset (reset asynchrone), \<prefix\>\_reset (reset synchrone)
  - pour les nresets : \<prefix\>\_aresetn (reset asynchrone), \<prefix\>\_resetn (reset synchrone)

- Assurez vous bien de bufferiser les sorties de vos IPs. Cela vous permettra d'éviter leur metastabilité.

