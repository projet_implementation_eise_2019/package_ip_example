library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

entity simple_adder_tb is
end entity;

architecture behav of simple_adder_tb is 
  constant enable_verif : boolean := true;
  constant clock_period : time := 10 ns;

  signal aclk    : std_logic := '1';
  signal aresetn : std_logic := '0';
  signal s0_axis_tdata , s1_axis_tdata , m_axis_tdata  : std_logic_vector(31 downto 0) := (others=>'0');
  signal s0_axis_tvalid, s1_axis_tvalid, m_axis_tvalid : std_logic := '0';
  signal s0_axis_tlast , s1_axis_tlast , m_axis_tlast  : std_logic := '0';
  signal s0_axis_tready, s1_axis_tready, m_axis_tready : std_logic := '0';
  
  component simple_adder is
    port( aclk    : in std_logic; -- clock shared by master and slaves
          aresetn : in std_logic; -- async active low reset
          
          -- first slave : first operand
          s0_axis_tdata  : in  std_logic_vector(31 downto 0);
          s0_axis_tvalid : in  std_logic;
          s0_axis_tready : out std_logic;
          s0_axis_tlast  : in  std_logic;
          
          -- second slave : second operand
          s1_axis_tdata  : in  std_logic_vector(31 downto 0);
          s1_axis_tvalid : in  std_logic;
          s1_axis_tready : out std_logic;
          s1_axis_tlast  : in  std_logic;
          
          -- master : result
          m_axis_tdata  : out std_logic_vector(31 downto 0);
          m_axis_tvalid : out std_logic;
          m_axis_tready : in  std_logic;
          m_axis_tlast  : out std_logic);
  end component;

  signal cnt0 : unsigned(31 downto 0) := (others=>'0');
  signal cnt1 : unsigned(31 downto 0) := to_unsigned(256,32);

  signal result_d, result_q : unsigned(31 downto 0) := (others=>'0');
  signal iresult : integer := 0;
  signal ireshw : integer := 0;
  signal first_d, first_q : std_logic := '0';
begin

clock_gen: process -- 100 MHz 
begin
  aclk <= '1';
  wait for clock_period / 2;
  aclk <= '0';
  wait for clock_period / 2;
end process;

rst_gen: process
begin
  aresetn <= '0';
  wait for clock_period * 10;
  aresetn <= '1';
  wait;
end process;

stimulus: process(aclk, aresetn)
begin
  if(aresetn = '0') then
    cnt0 <= (others => '0');
    cnt1 <= to_unsigned(256, 32);
  elsif(rising_edge(aclk)) then
    if(s0_axis_tvalid = '1' and s0_axis_tready = '1' and s1_axis_tready = '1') then 
      cnt0 <= cnt0 + 1;
      cnt1 <= cnt1 + 1;
    end if;
  end if;
end process;

verif_gen: if(enable_verif) generate
  iresult <= to_integer(result_q);
  ireshw  <=      to_integer(unsigned(m_axis_tdata)) when m_axis_tvalid = '1'
             else 0;
  result_d <=      cnt0 + cnt1 when first_q = '1'
              else (others=>'0');
  first_d <= aresetn;
  verif_proc : process(aclk, aresetn)
  begin
    if(aresetn = '0') then
      result_q <= (others=>'0');
      first_q <= '0';
    elsif(rising_edge(aclk)) then
      assert iresult = ireshw
        report "expected: " & integer'image(iresult) & " got: " & integer'image(ireshw)
        severity warning;
      result_q <= result_d;
      first_q <= first_d;
    end if;
  end process;
end generate;

s0_axis_tdata <= std_logic_vector(cnt0);
s1_axis_tdata <= std_logic_vector(cnt1);
s0_axis_tlast <= cnt0(3);
s1_axis_tlast <= cnt1(3);
m_axis_tready <= aresetn;
s0_axis_tvalid <= aresetn;
s1_axis_tvalid <= aresetn;

inst: simple_adder
  port map( aclk    => aclk
          , aresetn => aresetn
          , s0_axis_tdata  => s0_axis_tdata
          , s0_axis_tvalid => s0_axis_tvalid
          , s0_axis_tlast  => s0_axis_tlast
          , s0_axis_tready => s0_axis_tready
          , s1_axis_tdata  => s1_axis_tdata
          , s1_axis_tvalid => s1_axis_tvalid
          , s1_axis_tlast  => s1_axis_tlast
          , s1_axis_tready => s1_axis_tready
          , m_axis_tdata   => m_axis_tdata
          , m_axis_tvalid  => m_axis_tvalid
          , m_axis_tlast   => m_axis_tlast
          , m_axis_tready  => m_axis_tready);

end architecture;
