SYNTH_FILES = $(shell find -name *.vhd | grep synth/)
SIM_FILES = $(shell find -name *.vhd | grep sim/)

all: ghdl_all vivado_all

include mk/ghdl.mk
include mk/vivado.mk

clean: ghdl_clean vivado_clean
