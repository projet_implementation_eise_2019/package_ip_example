library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity simple_adder is
  port( aclk    : in std_logic; -- clock shared by master and slaves
        aresetn : in std_logic; -- async active low reset
        
        -- first slave : first operand
        s0_axis_tdata  : in  std_logic_vector(31 downto 0);
        s0_axis_tvalid : in  std_logic;
        s0_axis_tready : out std_logic;
        s0_axis_tlast  : in  std_logic;
        
        -- second slave : second operand
        s1_axis_tdata  : in  std_logic_vector(31 downto 0);
        s1_axis_tvalid : in  std_logic;
        s1_axis_tready : out std_logic;
        s1_axis_tlast  : in  std_logic;
        
        -- master : result
        m_axis_tdata  : out std_logic_vector(31 downto 0);
        m_axis_tvalid : out std_logic;
        m_axis_tready : in  std_logic;
        m_axis_tlast  : out std_logic);
end entity;

architecture behav of simple_adder is 
  -- notation:
  -- *_d -> behavioral signal (input of register)
  -- *_q -> sequential signal (output of register)
  signal data_d, data_q   : unsigned(31 downto 0);
  signal valid_d, valid_q : std_logic;
  signal last_d, last_q   : std_logic;
  signal ready_d, ready_q : std_logic;
begin
  -- tdata, tvalid, tlast from slaves ; tready from master
  data_d <=      unsigned(s0_axis_tdata) + unsigned(s1_axis_tdata) when valid_d = '1' and ready_q = '1'
            else data_q;
  last_d <= s0_axis_tlast and s1_axis_tlast;
  valid_d <= s0_axis_tvalid and s1_axis_tvalid;
  ready_d <= m_axis_tready;

  -- master outputs
  m_axis_tdata <= std_logic_vector(data_q);
  m_axis_tlast <= last_q;
  m_axis_tvalid <= valid_q;
  
  -- slaves outputs
  s0_axis_tready <= ready_q;
  s1_axis_tready <= ready_q;

-- sequential process for bufferization
process(aclk, aresetn)
begin
  if(aresetn = '0') then
    data_q <= (others=>'0');
    valid_q <= '0';
    last_q <= '0';
    ready_q <= '0';
  elsif(rising_edge(aclk)) then
    data_q <= data_d;
    valid_q <= valid_d;
    last_q <= last_d;
    ready_q <= ready_d;
  end if;
end process;
end architecture;
